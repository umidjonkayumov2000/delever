// import logo from './logo.svg';
import './App.css';
import { Routes, Route } from 'react-router-dom'

import { routes } from './routers'
// async permission = []
function App() {
  console.log(routes)
  // const generator = ({ children }) => {
  //   if (!children.length || children) {
  //     return 
  //   }
  //   generator()
  // }
  return (
    <div className="App">
      <Routes>
        {routes.map(({ component: Component, path, title, children }, i) =>
          <Route key={i} path={path} element={<Component />}>
            {
              children ? children.map((el, f) =>
                <Route path={el.path} key={f + 40} element={<el.component />} />
              ) : ''
            }
          </Route>
        )}
      </Routes>
    </div>
  );
}

export default App;
