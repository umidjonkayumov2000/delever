import { Outlet } from "react-router-dom";
import BootcampLogo from "../../assets/images/Bootcamp-logo.svg";
import LoginPC from "../../assets/images/Login-PC.png";
import { Checkbox } from "@mui/material";
import LoginUser from "../../assets/images/Login-user.svg";
import LoginPassword from "../../assets/images/Login-password.svg";

export default function Login({ logo, title }) {
  return (
    <div className="flex justify-center">
      <div className="w-1/2 flex flex-col justify-center align-middle h-screen bg-[#0067F4]">
        <img
          className="absolute top-[30px] left-[30px]"
          src={BootcampLogo}
          alt="Bootcamp"
        />
        <div className="flex items-center justify-center">
          <img className="md:w-[420px] 2xl:w-[690px]" src={LoginPC} alt="PC" />
        </div>
      </div>
      <div className="w-1/2 h-screen bg-[#fff] flex align-center justify-center">
        <div className="w-[500px] mx-auto flex flex-col items-start align-center justify-center">
          {/* Heading */}
          <h1 className="font-[36px] font-bold text-[#001A49] mb-[40px]">
            Вход в платформу
          </h1>
          {/* Form */}
          <form className="flex flex-col items-start w-[100%]">
            {/* Login */}
            <div className="relative w-[100%]">
              <img className="absolute top-[31px] left-[11px] w-[27px]" src={LoginUser} alt="user" />
              <span className="flex mb-[8px] text-[#1A2024] font-[16px] font-[600]">
                Имя пользователья
                <article className="text-[#F76659]">*</article>
              </span>
              <input
                className="block mb-[16px] w-[100%] pt-[12px] pb-[12px] pr-[12px] pl-[48px] rounded-md border border-gray-700"
                type="text"
                placeholder="Арина Соколова"
              />
            </div>
            {/* Password */}
            <div className="relative w-[100%]">
              <img className="absolute top-[33px] left-[11px]" src={LoginPassword} alt="password" />
              <span className="flex mb-[8px] text-[#1A2024] font-[16px] font-[600]">
                Пароль
                <article className="text-[#F76659]">*</article>
              </span>
              <input
                className="block mb-[24px] w-[100%] pt-[12px] pb-[12px] pr-[12px] pl-[48px] rounded-md border border-gray-700"
                type="password"
                placeholder="Введите пароль"
              />
            </div>
            {/* Checkbox */}
            <div className="flex items-center mb-[24px]">
              <Checkbox />
              <span className="text-[#6E8BB7] font-[500] text-[16px]">
                Запомнить меня
              </span>
            </div>
            {/* Button */}
            <button className="bg-[#0067F4] w-[100%] p-[13px] text-[#fff] rounded-md text-[17px]">
              Войти
            </button>
          </form>

          <Outlet />
        </div>
      </div>
    </div>
  );
}
