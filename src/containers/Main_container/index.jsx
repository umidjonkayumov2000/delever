import { Outlet } from 'react-router-dom'

import Navbar from '@layouts/Navbar'
import Sidebar from '@layouts/Sidebar'
// import Wrapper from '@layouts/Wrapper'

export default function MainContainer () {
    return (
        <div className="flex bg-white">
            <div className="z-10 mr-auto">
                <Sidebar/>
            </div>
            <div className="h-full w-[95%] shrink">
                <Navbar/>
                <div className="flex-none bg-orange-50">{ <Outlet/>}</div>
            </div>
        </div>
    )
}