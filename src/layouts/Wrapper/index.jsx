export default function App({ children }) {
  return (
    <div>
      {/* <Navbar /> */}
      <div
        style={{ height: "auto" }}
        className="flex justify-end w-full bg-violet-500"
      >
        {children}
      </div>
    </div>
  );
}
