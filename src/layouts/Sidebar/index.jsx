import { NavLink } from "react-router-dom";
import "./Sidebar.css";
import Logo from "../../assets/images/logo.svg";
import Notification from "../../assets/images/sidebar-images/notification.svg";
import Avatar from "../../assets/images/sidebar-images/avatar.svg";
import { ReactComponent as StatisticSVG } from "../../assets/images/sidebar-images/statistic.svg";
import { ReactComponent as UsersSVG } from "../../assets/images/sidebar-images/users.svg";
import { ReactComponent as ProductSVG } from "../../assets/images/sidebar-images/product.svg";
import { ReactComponent as TableSVG } from "../../assets/images/sidebar-images/table.svg";
import { ReactComponent as ShopSVG } from "../../assets/images/sidebar-images/shop.svg";
import { ReactComponent as RateSVG } from "../../assets/images/sidebar-images/rate.svg";
import { ReactComponent as HistorySVG } from "../../assets/images/sidebar-images/history.svg";
import { ReactComponent as SettingsSVG } from "../../assets/images/sidebar-images/settings.svg";

export default function App({ children }) {
  return (
    <div className="fixed flex flex-col items-center w-[64px] h-screen bg-[#fff] p-[12px]">
      {/* Logo */}
      <img className="w-[40px] mb-[24px]" src={Logo} alt="logo" />
      {/* Nav */}
      <nav className="flex flex-col items-center mb-auto bg-[#F0F3F8] rounded-md">
        {/* NavList */}
        <NavLink
          className={(navData) =>
            navData.isActive
              ? "mb-[14px] active"
              : "mb-[14px] bg-[#4094F7] p-[10px] rounded-md"
          }
          to="/stat"
        >
          <StatisticSVG
            className="nav-icon"
            width="20"
            height="20"
            src={StatisticSVG}
            alt="Statistic"
          />
        </NavLink>
        {/* Users */}
        <NavLink className="mb-[14px]" to="/users">
          <UsersSVG
            className="nav-icon"
            width="20"
            height="20"
            src={UsersSVG}
            alt="Statistic"
          />
        </NavLink>
        {/* Products */}
        <NavLink className="mb-[14px]" to="/products">
          <ProductSVG
            className="nav-icon"
            width="20"
            height="20"
            src={ProductSVG}
            alt="Statistic"
          />
        </NavLink>
        {/* Table */}
        <NavLink className="mb-[14px]" width="20" height="20" to="/table">
          <TableSVG
            className="nav-icon"
            width="20"
            height="20"
            src={TableSVG}
            alt="Statistic"
          />
        </NavLink>
        {/* Shop */}
        <NavLink className="mb-[14px]" to="/shop">
          <ShopSVG
            className="nav-icon"
            width="20"
            height="20"
            src={ShopSVG}
            alt="Statistic"
          />
        </NavLink>
        {/* Rate */}
        <NavLink className="mb-[14px]" to="/rate">
          <RateSVG
            className="nav-icon"
            width="20"
            height="20"
            src={RateSVG}
            alt="Statistic"
          />
        </NavLink>
        {/* History */}
        <NavLink className="mb-[14px]" to="/history">
          <HistorySVG
            className="nav-icon"
            width="20"
            height="20"
            src={HistorySVG}
            alt="Statistic"
          />
        </NavLink>
        {/* Settings */}
        <NavLink className="mb-[14px]" to="/settings">
          <SettingsSVG
            className="nav-icon"
            width="20"
            height="20"
            src={SettingsSVG}
            alt="Statistic"
          />
        </NavLink>
      </nav>

      <div className="">
        <button className="mb-[26px]">
          <img src={Notification} alt="notification" />
        </button>

        <button>
          <img src={Avatar} alt="avatar" />
        </button>
      </div>
    </div>
  );
}
