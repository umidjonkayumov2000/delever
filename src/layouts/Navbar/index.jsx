
export default function App ({ children }) {
    return (
        <section className="flex items-center h-[55px] bg-white bg-w">
            <h1 className="font-[600] font-[24px] text-[#303940] pl-[32px] border-l-[1px] border-l-gray-300">Dashboard</h1>
        </section>
    )
}